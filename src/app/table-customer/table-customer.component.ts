import { ClassifyService } from './../classify.service';
import { TableCustomerService } from './../table-customer.service';
import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '../customer';


@Component({
  selector: 'app-table-customer',
  templateUrl: './table-customer.component.html',
  styleUrls: ['./table-customer.component.css']
})
export class TableCustomerComponent implements OnInit {

  customers=[];
  displayedColumns: string[] = ['name','phone','email','more_than_1_card','predict','edit','delete'];
  customers$;
  new;
  resultPredict;

  rowToEdit:number = -1; //אתחול אינדקס השורה עם מינוס 1 כדי שלא נהיה במצב של עריכה בשום שורה
  customerToEdit:Customer = {
  id:null,
  age:null,
  more_than_1_card:null,
  student:null,
  main_city_sail:null,
  Utilizing_card_1:null,
  Utilizing_card_2:null,
  Utilizing_card_3:null,
  Utilizing_card_4:null,
  perc_private:null,
   perc_proff:null,
   name:null,
   email:null,
   phone:null
  };

  addBookFormOpen = false;

  lastCustomerArrived //דפדוף
  firstCustomerArrived;//דפדוף

  @Input() name:string;
  @Input() email:string;
  @Input() phone:string;

  moveToEditState(index){ //הפונקציה מאתרת את מספר השורה שבה הפריט שנעדכן נמצא
    this.customerToEdit.age = this.customers[index].age;
    this.customerToEdit.more_than_1_card = this.customers[index].more_than_1_card;
    // this.customerToEdit.student = this.customers[index].student;
    // this.customerToEdit.main_city_sail = this.customers[index].main_city_sail;
    // this.customerToEdit.Utilizing_card_1 = this.customers[index].Utilizing_card_1;
    // this.customerToEdit.Utilizing_card_2 = this.customers[index].Utilizing_card_2;
    // this.customerToEdit.Utilizing_card_3 = this.customers[index].Utilizing_card_3;
    // this.customerToEdit.Utilizing_card_4 = this.customers[index].Utilizing_card_4;
    // this.customerToEdit.perc_private = this.customers[index].perc_private;
    // this.customerToEdit.perc_proff = this.customers[index].perc_proff;
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.email = this.customers[index].email;
    this.customerToEdit.phone = this.customers[index].phone;
    this.rowToEdit = index; 
  }

  constructor(private TableCustomerService:TableCustomerService, private ClassifyService:ClassifyService) { }


  // updateCustomer(){
  //   let id = this.customers[this.rowToEdit].id;
  //   this.TableCustomerService.editCustomer(id, this.customerToEdit.age, this.customerToEdit.more_than_1_card, 
  //     this.customerToEdit.student,this.customerToEdit.main_city_sail, this.customerToEdit.Utilizing_card_1,
  //     this.customerToEdit.Utilizing_card_2,this.customerToEdit.Utilizing_card_3,this.customerToEdit.Utilizing_card_4,
  //     this.customerToEdit.perc_private, this.customerToEdit.perc_proff, this.customerToEdit.name,
  //     this.customerToEdit.email,this.customerToEdit.phone);
  //   this.rowToEdit = null;
  // }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.TableCustomerService.editCustomer(id,this.customerToEdit.more_than_1_card, this.customerToEdit.name,
      this.customerToEdit.email,this.customerToEdit.phone);
    this.rowToEdit = null;
  }

  predict(){
    this.ClassifyService.classify(this.customers).subscribe(
                   (res:number[])=> {
                     console.log(res);
                    //  if (res > 0.5){
                    //    customer.predict = 'will not leave';
                    //  }
                    //  if (res < 0.5) {
                    //    customer.predict = 'will leave';
                    //  }
                    for (let i=0;i<res.length; i++){
                      const customerPrediction = res[i];
                      const predict = customerPrediction>0.5?'will not leave':'will leave';
                      const customer:Customer = this.customers[i];
                      const {id} = customer;
                      this.TableCustomerService.updateCustomer(id, predict);

                    }
                    }
                 )         
  }


  deleteCustomer(index){
    let id = this.customers[index].id;
    this.TableCustomerService.deleteCustomer(id);
  }


  ngOnInit(): void {

          this.customers$ = this.TableCustomerService.getCustomers(null);
          this.customers$.subscribe(
            docs => {  
              this.lastCustomerArrived = docs[docs.length-1].payload.doc;       
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();

                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }   
            //  console.log(this.customers);
                     
            }
            
          )
      }

  //דפדוף
nextPage(){
  this.customers$ = this.TableCustomerService.getCustomers(this.lastCustomerArrived); 
  this.customers$.subscribe( 
    docs => { 
      this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
      this.firstCustomerArrived = docs[0].payload.doc; 
      this.customers = []; 
      for(let document of docs){ 
        const customer:Customer = document.payload.doc.data(); 
        customer.id = document.payload.doc.id; 
        this.customers.push(customer); 
      } 
    }
    )
    }



//דפדוף
previewsPage(){
this.customers$ = this.TableCustomerService.getCustomers2(this.firstCustomerArrived); 
this.customers$.subscribe( 
docs => { 
  this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
  this.firstCustomerArrived = docs[0].payload.doc; 
  this.customers = []; 
  for(let document of docs){ 
    const customer:Customer = document.payload.doc.data(); 
    customer.id = document.payload.doc.id; 
    this.customers.push(customer); 
  } 
}
)
}
//getCustomersLeave(){
  //   this.customers$ = this.TableCustomerService.getCustomersLeave(); 
  //   this.customers$.subscribe(
  //     docs =>{
  //       console.log(docs);
  //       this.customers = [];
  //       for(let document of docs){
  //        // console.log(document);
  //         const customer:Customer= document.payload.doc.data();
  //         customer.id = document.payload.doc.id; 
          
  //   //  this.TableCustomerService.updateCustomer('PFhCEC2CReMlJ1dfnESQ', 'jg');
  //       this.customers.push(customer);
  //       }
        
  //       console.log(this.customers);
  //     }
  //   )
  // }
  

      /*this.authService.getUser().subscribe( //הפונקציה מחזירה לנו את האיידי של היוזר שמחובר
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId); //יצירת האובסרבבול
          this.customers$.subscribe( //הרשמה לאובסרבבול
            docs => {         
              this.customers = []; //נאפס את מערך הלקוחות
              var i = 0;
              for (let document of docs) { //עבור כל דיווח שמתקבל אודות השינוי בדאטה בייס, נעדכן את מערך הלקוחות ובעזרת לולאה נכניס את המשתנים
                console.log(i++); 
                const customer:Customer = document.payload.doc.data(); //עוזר לנו להבין את השדות בכל דוקיומנט ולאפשר לשלוף מהם את הערכים
                if(customer.result){ //בודק האם נשמרה עבור הלקוח תוצאת החיזוי
                  customer.saved = true; //אם כן , נעדכן שהשמירה בוצעה
                }
                customer.id = document.payload.doc.id; //האיידי הוא לא חלק מהדאטה, אותו צריך להביא בנפרד
                   this.customers.push(customer); //נוסיף למערך הריק שיצרנו את כל הלקוחות
              }                        
            }
          )
      })
  }*/


//   try(){
//     this.customers$ = this.TableCustomerService.getCustomers(); 
//     this.customers$.subscribe(
//       docs =>{
//         this.customers = [];
//         for(let document of docs){
//          // console.log(document);
//           const customer:Customer= document.payload.doc.data();
//           customer.id = document.payload.doc.id; 
//           this.customers.push(customer);
//       }
//     }
//   )

//  // let data = JSON.stringify(this.customers);
//   //  console.log("data:",data);
//    // console.log("data-type:",typeof(data));
//    // data = JSON.loads(JSON.dumps(data))
//   //  console.log("this.customer2",this.customers)
//   //     for(let customer in this.customers){
//   //       var x;
//   //       const yyy = JSON.parse(customer);
//   //       console.log("customer",customer);
//   //       for (x in yyy) {
//   //         console.log("DFgfg");
//   //       console.log(typeof(yyy));
//   //       console.log(yyy[x]);
//   //       }
//     //     for (x in customer) {

//                    this.ClassifyService.classify(2,1, 0, 0, 0, 0, 0, 0, 0, 0).subscribe(
//              res=> {
//                console.log(res);
//               //  if (res > 0.5){
//               //    customer.predict = 'will not leave';
//               //  }
//               //  if (res < 0.5) {
//               //    customer.predict = 'will leave';
//               //  }
//               }
//            )
//        this.TableCustomerService.updateCustomer('PFhCEC2CReMlJ1dfnESQ', 'jg');
//      }
//    // }
//   }

//   try2(){
//     const myObj = JSON.parse('{"name":"1", "age":2, "car":"3"}');
//     var x;
//     for (x in myObj) {
//    console.log(myObj[x]);
  
// }
// }


  // ngOnInit() {
  //   this.customers$ = this.TableCustomerService.getCustomers(); 
  //   this.customers$.subscribe(
  //     docs =>{
  //       console.log(docs);
  //       this.customers = [];
  //       for(let document of docs){
  //        // console.log(document);
  //         const customer:Customer= document.payload.doc.data();
  //         customer.id = document.payload.doc.id; 
          
  //   //  this.TableCustomerService.updateCustomer('PFhCEC2CReMlJ1dfnESQ', 'jg');
  //       this.customers.push(customer);
  //       }
        
  //       console.log(this.customers);
  //     }
  //   )
      
  // }

}






