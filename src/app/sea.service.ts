import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeaService {


  constructor(private http: HttpClient) { }


  //נוסיף משתנים
  //התחברות לקריאת האייפיאיי
  private url = "https://api.windy.com/api/point-forecast/v2"; //האנדפוינט למעשה
  //private KEY = "SaEeTq7ogsvMe8D3ppU8Ze73hgIb3E5r"; //האייפיאיי קיי האישי שלי

  classify(lon:number, lat:number):Observable<any>{
    let json = {
          "lat": lat,
          "lon": lon,
          "model": "gfs",
          "parameters": ["temp", "wind", "precip", "lclouds", "dewpoint", "rh", "pressure", "cape"],
          "levels": ["surface", "800h", "300h"],
          "key": "zYNSmQxcXjWGcbzCcIpReqrue4T449VF"
    }
    // let body = JSON.stringify(json);
    return this.http.post<any>(this.url,json).pipe(
      map(res => {
        return res;       
      })
    );      
  }
}
