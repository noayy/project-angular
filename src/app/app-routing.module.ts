import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { HomeComponent } from './home/home.component';
import { LeaveCustomersComponent } from './leave-customers/leave-customers.component';
import { LoginComponent } from './login/login.component';
import { SeaComponent } from './sea/sea.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TableCustomerComponent } from './table-customer/table-customer.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'customerTable', component: TableCustomerComponent },
  { path: 'addcustomer', component: AddCustomerComponent },
  { path: 'seaCondition', component: SeaComponent },
  { path: 'leavecustomers', component: LeaveCustomersComponent },

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
