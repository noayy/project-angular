import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from './interfaces/user';
import { map } from 'rxjs/operators';

const GuideEmail = 'guide@guide.com';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:Observable<User | null>; //יצרנו אינטרפייס של יוזר. האובסרבל יהיה מסוג יוזר או מסוג נאל

  isSecretary: Observable<boolean>;
  isGuide: Observable<boolean>;

  isSecretaryPipe(): Observable<boolean>
  {
    return this.user.pipe(
      map(user => user && user.email !== GuideEmail)
    );
  }

  isGuidePipe(): Observable<boolean>
  {
    return this.user.pipe(
      map(user => user && user.email == GuideEmail)
    );
  }

  logout(){
    this.afAuth.signOut();
  }

  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email,password)
  }

  getUser():Observable<User | null>{
    return this.user;
    // הפונקציה מחזירה אובסרבל של יוזר שמחובר כעת, ואם אין היא מחזירה נאל
  }

  signUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password)
  }


  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
          this.user = this.afAuth.authState;
          this.isSecretary = this.isSecretaryPipe();
          this.isGuide = this.isGuidePipe();
          console.log("auth service constructr worked");
    }
}
