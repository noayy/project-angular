import { Customer } from './../customer';
import { TableCustomerService } from './../table-customer.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'addcustomer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {



  age;
  more_than_1_card;
  student;
  main_city_sail;
  Utilizing_card_1;
  Utilizing_card_2;
  Utilizing_card_3;
  Utilizing_card_4;
  perc_private;
  perc_proff;
  name;
  email;
  phone;
  predict="";
  selected;


  constructor(private router:Router,
    public auth:AuthService,
    private tableCustomerService:TableCustomerService) { }

  save(){
      this.tableCustomerService.addcustomer(this.age,this.more_than_1_card, this.student, this.main_city_sail,this.Utilizing_card_1,
        this.Utilizing_card_2,this.Utilizing_card_3,this.Utilizing_card_4,
        this.perc_private,this.perc_proff,this.name, this.email,this.phone,this.predict); 
      this.router.navigate(['/customerTable']); 
  }

//   save(){
//     console.log(this.age);
//     this.tableCustomerService.add(this.age); 
// }

  ngOnInit(): void {
  }

}
