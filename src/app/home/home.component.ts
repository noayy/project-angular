import { ClassifyService } from './../classify.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
/*student:string = 'yes';
  sex:string = 'male';
  predictVar;

  predict(){
    this.ClassifyService.classify(this.sex,this.student).subscribe(
      res=> {
        this.predictVar = res;
      }
    )
  }
*/

  constructor(public AuthService:AuthService, public ClassifyService:ClassifyService) { }

  ngOnInit(): void {
  }

}
