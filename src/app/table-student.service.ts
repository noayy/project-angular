import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TableStudentService {

  studentsCollection:AngularFirestoreCollection;

  deleteStudent(userId, id){
    this.db.doc(`users/${userId}/students/${id}`).delete()
  }

  getStudents(userId){
    this.studentsCollection = this.db.collection(`users/${userId}/students`, 
    ref => ref.orderBy('index', 'desc'));
    return this.studentsCollection.snapshotChanges().pipe(map( 
       collection => collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id; 
          console.log(data)
          return data; 
        }
        )
      )) 

  }

  constructor(private db:AngularFirestore) { }
}
